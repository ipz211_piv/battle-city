﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using MainLibrary;


namespace Game
{
    public partial class programSpace : Form
    {

        Tank tank;
        WatermelonBase watermelonbase;
        TankBullet tankBullet = new TankBullet();
        Timer EnemyTimer = new Timer();

        int EnemyCount = 0;
        int DestroyedEnemies = -4;
        int LevelNUM = 1;

        List<Enemy> enemies = new List<Enemy>();

        Map map = new Map();
        MessageHandler message = new MessageHandler();
        public programSpace()
        {
            InitializeComponent();
            foreach (Control tree in gameSpace.Controls)
            {
                if (tree.GetType() == typeof(PictureBox))
                {
                    PictureBox pictureBox = tree as PictureBox;
                    if (pictureBox.BackColor == Color.Green)
                    {
                        pictureBox.BringToFront();
                    }
                }
            }
            labelBestResult.Text = Convert.ToString(CheckBestResult());

            Map map = new Map();
            map.BuildMap(ref gameSpace);
            tank = new Tank(ref gameSpace);
            watermelonbase = new WatermelonBase(ref gameSpace);
            labelHP.Text = Convert.ToString(tank.HP);
            EnemyTimer.Interval = 3000;
            EnemyTimer.Tick += new EventHandler(EnemyTimerTick);
            EnemyTimer.Start();
            Timer MAINCONTROLTimer = new Timer();
            MAINCONTROLTimer.Interval = 50;
            MAINCONTROLTimer.Tick += new EventHandler(MAINCONTROLTimerTick);
            MAINCONTROLTimer.Start();
            
        }
        public void MAINCONTROLTimerTick(object sender, EventArgs e)
        {
            CheckAndChange(tank.Score);
            if ((tank.TankPictureBox.IsDisposed|| watermelonbase.Tag == "GAMEOVER") && tank.HP > 0)
            {
                tank.HP--;
                if (tank.HP > 0&& watermelonbase.Tag != "GAMEOVER")
                    tank = new Tank(tank, ref gameSpace);
                else
                {
                    
                    message.GAMEOVER(ref panelInfo);
                    EnemyTimer.Stop();
                }
            }
            labelHP.Text = Convert.ToString(tank.HP);
            labelScore.Text = Convert.ToString(tank.Score);

        }
        public void EnemyTimerTick(object sender, EventArgs e)
        {
           
            if (EnemyCount < 4 && DestroyedEnemies < 6)
            {
                
                enemies.Add(new Enemy(ref gameSpace));
                DestroyedEnemies++;
                label1.Text = $"{10-(DestroyedEnemies + 4)}";
            }
            EnemyCount = 0;
            foreach (Control Enemy in gameSpace.Controls)
            {
                if (Enemy.GetType() == typeof(PictureBox))
                {
                    PictureBox obj = Enemy as PictureBox;
                    if (obj.Tag == "StandartEnemy" || obj.Tag == "FastEnemy" || obj.Tag == "ArmoredEnemy1"
                            || obj.Tag == "ArmoredEnemy2" || obj.Tag == "ArmoredEnemy3" || obj.Tag == "ArmoredEnemy4"||obj.Tag == "PowerfullEnemy")
                    {
                        EnemyCount++;
                    }
                }
            }
            if (EnemyCount == 0 && DestroyedEnemies >= 3)
            {
                if (LevelNUM == 4)
                    LevelNUM = 1;
                else LevelNUM++;
                tank.TankPictureBox.Location = new Point(200, 600);
                message.WIN(ref panelInfo);
                EnemyTimer.Stop();
                enemies.Clear();
            }
        }

        private void CheckAndChange(int result) {
            string path = Directory.GetCurrentDirectory() + @"\Result.bin";

            if (CheckBestResult() < result)
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.OpenOrCreate)))
                {
                    writer.Write(result); 
                }
            }
        }
        private int CheckBestResult()
        {
            string path = Directory.GetCurrentDirectory() + @"\Result.bin";
            int Result;
           
            using (BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Open)))
            {
                Result = reader.ReadInt32();
            }
            return Result;
            
        }

        private void programSpace_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Right || e.KeyCode == Keys.Left || e.KeyCode == Keys.Up || e.KeyCode == Keys.Down) && tank.HP > 0&& !(EnemyCount == 0 && DestroyedEnemies >= 3) && watermelonbase.Tag != "GAMEOVER")
            {
                tank.Tank_KeyDown(e, gameSpace, ref tank.TankPictureBox);

            }
            if (e.KeyCode == Keys.Space && tank.HP > 0&& watermelonbase.Tag != "GAMEOVER" && !(EnemyCount == 0 && DestroyedEnemies >= 3))
            {
                tankBullet.TankBulletSpace(ref EnemyCount, ref gameSpace, ref tank);
            }
            if (e.KeyCode == Keys.Enter && (watermelonbase.Tag == "GAMEOVER" || tank.HP <= 0))
            {
                Application.Exit();
            }
            if (e.KeyCode == Keys.Enter && EnemyCount == 0 && DestroyedEnemies >= 3)
            {
                label1.Text = "10";
                message.Clear(ref panelInfo);
                EnemyCount = 0;
                DestroyedEnemies = -4;
                map.RebuildMap(ref gameSpace, LevelNUM);
                EnemyTimer.Start();
            }
            if (e.KeyCode == Keys.P )
            {
                System.Threading.Thread.Sleep(500);
            }
        }


    }
}
