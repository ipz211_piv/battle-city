﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace MainLibrary
{
    public class Map
    {
        Point[] MapPoints = new Point[26 * 26];
        int[] map=new int[26*26];
        public PictureBox MapBlock(int n)
        {
            PictureBox NewpictureBox = new PictureBox();
            NewpictureBox.Name = $"Block_{n}";
            if (n == 1)
            {
                NewpictureBox.BackgroundImage = Properties.Resources.Brick;
                NewpictureBox.Tag = "Brick";
            }
            else if (n == 2)
            {
                NewpictureBox.BackgroundImage = Properties.Resources.Steel;
                NewpictureBox.Tag = "Steel";
            }
            else if (n == 3)
            {
                NewpictureBox.BackgroundImage = Properties.Resources.Tree;
                NewpictureBox.Tag = "Tree";
            }
            else if (n == 4)
            {
                NewpictureBox.BackgroundImage = Properties.Resources.Water;
                NewpictureBox.Tag = "Water";
            }
            else if (n == 5)
            {
                NewpictureBox.BackgroundImage = Properties.Resources.Ice;
                NewpictureBox.Tag = "Ice";
            }
            else return null;
            NewpictureBox.Size = new Size(25, 25);
            NewpictureBox.BackgroundImageLayout = ImageLayout.Stretch;

            return NewpictureBox;
        }
        public void BuildMap(ref Panel gameSpace, int number=1)
        {
            
            string level = "", path = Directory.GetCurrentDirectory()+ $@"\Level{number}.txt";
            
            string[] lines = File.ReadAllLines($@"{Directory.GetCurrentDirectory()}\Level{number}.txt");
            foreach (string s in lines)
            {
                level += s;
            }
            Regex regexImage = new Regex(@"(\d)", RegexOptions.Compiled);

            int h=0;
            foreach (Match match in regexImage.Matches(level))
            {
                map[h] += Convert.ToInt32(match.Value);
                h++;
            }
                
            level = "";


            for (int i = 0; i < 26; i++)
                for (int j = 0; j < 26; j++)
                    MapPoints[i * 26 + j] = new Point(j * 25, i * 25);

            for (int i = 0; i < map.Length; i++)
            {
                PictureBox block = MapBlock(map[i]);
                if (block == null)
                    continue;
                block.Location = MapPoints[i];
                gameSpace.Controls.Add(block);
            }
        }
        public void RebuildMap(ref Panel gameSpace, int number = 1)
        {
            map = new int[26 * 26];
            for (int i = 0; i < 13; i++)
                foreach (Control A in gameSpace.Controls)
                if (A.GetType() == typeof(PictureBox))
                {
                    PictureBox Block = A as PictureBox;
                    if(Block.Tag== "Brick"|| Block.Tag == "Steel" || Block.Tag == "Tree"|| Block.Tag == "Water" || Block.Tag == "Ice"||Block.Tag == "")
                     A.Dispose();
                }
            string level = "";
            string[] lines = File.ReadAllLines($@"{ Directory.GetCurrentDirectory()}\Level{ number}.txt");
            foreach (string s in lines)
            {
                level += s;
            }
            Regex regexImage = new Regex(@"(\d)", RegexOptions.Compiled);
            int h = 0;
            foreach (Match match in regexImage.Matches(level))
            {
                map[h] += Convert.ToInt32(match.Value);
                h++;
            }

            level = "";
            MapPoints = new Point[26 * 26];
            for (int i = 0; i < 26; i++)
                for (int j = 0; j < 26; j++)
                    MapPoints[i * 26 + j] = new Point(j * 25, i * 25);

            for (int i = 0; i < map.Length; i++)
            {
                PictureBox block = MapBlock(map[i]);
                if (block == null)
                    continue;
                block.Location = MapPoints[i];
                gameSpace.Controls.Add(block);
            }
        }

    }
}

    
