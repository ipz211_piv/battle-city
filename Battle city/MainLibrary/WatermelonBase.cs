﻿using System.Windows.Forms;
using System.Drawing;

namespace MainLibrary
{
    public class WatermelonBase : PictureBox
    {
        public int TankHP = 3;
        public WatermelonBase(ref Panel gameSpace)
        {
            this.Tag = "Base";
            this.Size = new Size(50, 50);
            this.BackgroundImage = Properties.Resources.Base1;
            this.Location = new Point(300, 600);
            gameSpace.Controls.Add(this);
            this.BackgroundImageLayout = ImageLayout.Stretch;

        }
    }
}
