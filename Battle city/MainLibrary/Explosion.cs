﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace MainLibrary
{
    public class Explosion : PictureBox
    {
        Timer ExplosionTimer;
        public Explosion(ref Panel gameSpace, Point location, bool typeBIG = false)
        {
            if (!typeBIG)
            {
                this.Size = new Size(17, 17);
                this.Location = location;
                ExplosionColision(ref gameSpace);
            }
            else
            {
                this.Size = new Size(50, 50);
                this.Location = location;
                ExplosionDestoyed(ref gameSpace);
            }
            
        }
        private void ExplosionColision(ref Panel gameSpace)
        {
            ExplosionTimer = new Timer();
            ExplosionTimer.Interval = 200;
            this.Image = Properties.Resources.Boom;
            gameSpace.Controls.Add(this);
            ExplosionTimer.Tick += new EventHandler(ExplosionTimerTick);
            ExplosionTimer.Start();
        }
        private void ExplosionDestoyed(ref Panel gameSpace)
        {
            ExplosionTimer = new Timer();
            ExplosionTimer.Interval = 250;
            this.Image = Properties.Resources.CaBoom;
            
            gameSpace.Controls.Add(this);
            ExplosionTimer.Tick += new EventHandler(ExplosionTimerTick);
            ExplosionTimer.Start();
        }
        public void ExplosionTimerTick(object sender, EventArgs e)
        {
            ExplosionTimer.Stop();
            this.Dispose();
            
        }
            
    }
}
