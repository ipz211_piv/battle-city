﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace MainLibrary
{
    public class TankBullet
    {
        Tank tanK = new Tank();
        public Panel gamespace=new Panel();
        bool TankBulletShooted = false;
        bool TankBulletUp = false;
        bool TankBulletDown = false;
        bool TankBulletLeft = false;
        bool TankBulletRight = false;
        GameSound sound = new GameSound();
        Explosion explosion;
        public void TankBulletSpace(ref int enemyCount, ref Panel gameSpace, ref Tank tank)
        {
            if (!TankBulletShooted)
            {
                sound.FirePlay();
                TankBulletShooted = true;
                TankShot(ref  enemyCount, ref gameSpace, ref tank);
            }
        }
         public void TankShot(ref int enemyCount,ref Panel gameSpace, ref Tank tank)
        {
            PictureBox BulletpictureBox = new PictureBox();
            Image img = Properties.Resources.Bullet;

            TankBulletDown = false;
            TankBulletUp = false;
            TankBulletLeft = false;
            TankBulletRight = false;
            if (tank.TankUp)
            {
                BulletpictureBox.Size = new Size(6, 10);
                BulletpictureBox.Location = new Point(tank.TankPictureBox.Left + tank.TankPictureBox.Width / 2 - 3, tank.TankPictureBox.Top + tank.TankPictureBox.Height / 2);
                TankBulletUp = true;
            }
            if (tank.TankDown)
            {
                BulletpictureBox.Size = new Size(6, 10);
                BulletpictureBox.Location = new Point(tank.TankPictureBox.Left + tank.TankPictureBox.Width / 2 - 3, tank.TankPictureBox.Top + tank.TankPictureBox.Height / 2);
                img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                TankBulletDown = true;
            }
            if (tank.TankLeft)
            {
                BulletpictureBox.Size = new Size(10, 6);
                BulletpictureBox.Location = new Point(tank.TankPictureBox.Left, tank.TankPictureBox.Top + tank.TankPictureBox.Height / 2 - 3);
                img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                TankBulletLeft = true;
            }
            if (tank.TankRight)
            {
                BulletpictureBox.Size = new Size(10, 6);
                BulletpictureBox.Location = new Point(tank.TankPictureBox.Left, tank.TankPictureBox.Top + tank.TankPictureBox.Height / 2 - 3);
                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                TankBulletRight = true;
            }
            BulletpictureBox.BackgroundImage = img;
            BulletpictureBox.BackgroundImageLayout = ImageLayout.Stretch;
            BulletpictureBox.Tag = "Bullet";
            gameSpace.Controls.Add(BulletpictureBox);
            BulletpictureBox.BringToFront();
            tank.TankPictureBox.BringToFront();
            foreach (Control time in gameSpace.Controls)
            {
                if (time.GetType() == typeof(PictureBox))
                {
                    PictureBox pictureBox = time as PictureBox;
                    if (pictureBox.Tag == "Tree")
                    {
                        pictureBox.BringToFront();
                    }
                }
            }
            gamespace = gameSpace;
            tanK = tank;
            
            Timer TankBulletTimer = new Timer();
            TankBulletTimer.Interval = 20;
            TankBulletTimer.Tick += new EventHandler(TankBulletTimerTick);
            TankBulletTimer.Start();

        }

        public void TankBulletTimerTick( object sender, EventArgs e)
        {
            foreach (Control time in gamespace.Controls)
            {
                if (time.GetType() == typeof(PictureBox))
                {
                    PictureBox pictureBox = time as PictureBox;
                    if (pictureBox.Tag == "Bullet")
                    {
                        if (TankBulletUp)
                        {
                            pictureBox.Top = pictureBox.Top - 10;
                        }
                        if (TankBulletDown)
                        {
                            pictureBox.Top = pictureBox.Top + 10;
                        }
                        if (TankBulletRight)
                        {
                            pictureBox.Left = pictureBox.Left + 10;
                        }
                        if (TankBulletLeft)
                        {
                            pictureBox.Left = pictureBox.Left - 10;
                        }
                        if (pictureBox.Top < 0 || pictureBox.Top > gamespace.Height - tanK.TankPictureBox.Height / 2 || pictureBox.Left < 0 || pictureBox.Left > gamespace.Width - tanK.TankPictureBox.Width / 2)
                        {
                            TankBulletDown = false;
                            TankBulletUp = false;
                            TankBulletLeft = false;
                            TankBulletRight = false;
                            TankBulletShooted = false;
                            sound.BlockColissionPlay();
                            explosion = new Explosion(ref gamespace, pictureBox.Location);
                            Timer tm = (Timer)sender;
                            tm.Dispose();
                            pictureBox.Dispose();
                        }

                        foreach (Control A in gamespace.Controls)
                        {

                            if (A.GetType() == typeof(PictureBox))
                            {
                                PictureBox NewpictureBox = A as PictureBox;
                                if (NewpictureBox.Visible && (NewpictureBox.Tag == "Brick" || NewpictureBox.Tag == "EnemyBullet")
                                    && pictureBox.Bounds.IntersectsWith(NewpictureBox.Bounds))
                                {
                                    TankBulletDown = false;
                                    TankBulletUp = false;
                                    TankBulletLeft = false;
                                    TankBulletRight = false;
                                    TankBulletShooted = false;
                                    Timer tm = (Timer)sender;
                                    sound.BlockColissionPlay();
                                    explosion = new Explosion(ref gamespace, pictureBox.Location);
                                    tm.Dispose();
                                    pictureBox.Dispose();
                                    NewpictureBox.Dispose();

                                }
                                if (NewpictureBox.Visible && (NewpictureBox.Tag == "Steel")
                                    && pictureBox.Bounds.IntersectsWith(NewpictureBox.Bounds))
                                {
                                    TankBulletDown = false;
                                    TankBulletUp = false;
                                    TankBulletLeft = false;
                                    TankBulletRight = false;
                                    TankBulletShooted = false;
                                    sound.BlockColissionPlay();
                                    explosion = new Explosion(ref gamespace, pictureBox.Location);
                                    Timer tm = (Timer)sender;
                                    tm.Dispose();
                                    pictureBox.Dispose();
                                }
                                if ((NewpictureBox.Tag == "StandartEnemy" || NewpictureBox.Tag == "FastEnemy" || NewpictureBox.Tag == "PowerfullEnemy")
                                    && pictureBox.Bounds.IntersectsWith(NewpictureBox.Bounds))
                                {
                                    TankBulletDown = false;
                                    TankBulletUp = false;
                                    TankBulletLeft = false;
                                    TankBulletRight = false;
                                    TankBulletShooted = false;
                                    if (NewpictureBox.Tag == "StandartEnemy")
                                        tanK.Score += 100;
                                    else if (NewpictureBox.Tag == "FastEnemy" || NewpictureBox.Tag == "PowerfullEnemy")
                                        tanK.Score += 150;
                                    Timer tm = (Timer)sender;
                                    sound.DestroyedPlay();
                                    explosion = new Explosion(ref gamespace, NewpictureBox.Location, true);
                                    tm.Dispose();
                                    pictureBox.Dispose();
                                    NewpictureBox.Dispose();
                                    NewpictureBox.Tag = "Dead";

                                }
                                if (NewpictureBox.Tag == "ArmoredEnemy4"
                                    && pictureBox.Bounds.IntersectsWith(NewpictureBox.Bounds))
                                {

                                    NewpictureBox.Tag = "ArmoredEnemy3";
                                    TankBulletDown = false;
                                    TankBulletUp = false;
                                    TankBulletLeft = false;
                                    TankBulletRight = false;
                                    TankBulletShooted = false;
                                    sound.ArmoredPenetrationPlay();
                                    explosion = new Explosion(ref gamespace, NewpictureBox.Location);
                                    Timer tm = (Timer)sender;
                                    tm.Dispose();
                                    pictureBox.Dispose();
                                }
                                else
                                if (NewpictureBox.Tag == "ArmoredEnemy3"
                                    && pictureBox.Bounds.IntersectsWith(NewpictureBox.Bounds))
                                {
                                    NewpictureBox.Tag = "ArmoredEnemy2";
                                    TankBulletDown = false;
                                    TankBulletUp = false;
                                    TankBulletLeft = false;
                                    TankBulletRight = false;
                                    TankBulletShooted = false;
                                    sound.ArmoredPenetrationPlay();
                                    explosion = new Explosion(ref gamespace, NewpictureBox.Location);
                                    Timer tm = (Timer)sender;
                                    tm.Dispose();
                                    pictureBox.Dispose();
                                }
                                else
                                if (NewpictureBox.Tag == "ArmoredEnemy2"
                                    && pictureBox.Bounds.IntersectsWith(NewpictureBox.Bounds))
                                {
                                    NewpictureBox.Tag = "ArmoredEnemy1";
                                    TankBulletDown = false;
                                    TankBulletUp = false;
                                    TankBulletLeft = false;
                                    TankBulletRight = false;
                                    TankBulletShooted = false;
                                    sound.ArmoredPenetrationPlay();
                                    explosion = new Explosion(ref gamespace, NewpictureBox.Location);
                                    Timer tm = (Timer)sender;
                                    tm.Dispose();
                                    pictureBox.Dispose();
                                }
                                else
                                if (NewpictureBox.Tag == "ArmoredEnemy1"
                                    && pictureBox.Bounds.IntersectsWith(NewpictureBox.Bounds))
                                {
                                    TankBulletDown = false;
                                    TankBulletUp = false;
                                    TankBulletLeft = false;
                                    TankBulletRight = false;
                                    TankBulletShooted = false;
                                    tanK.Score += 200;
                                    sound.DestroyedPlay();
                                    explosion = new Explosion(ref gamespace, NewpictureBox.Location, true);
                                    Timer tm = (Timer)sender;
                                    tm.Dispose();
                                    pictureBox.Dispose();
                                    NewpictureBox.Dispose();
                                    NewpictureBox.Tag = "Dead";
                                }

                            }
                            if (A.GetType() == typeof(WatermelonBase))
                            {
                                if (A.Visible && pictureBox.Bounds.IntersectsWith(A.Bounds))
                                {
                                    TankBulletShooted = false;
                                    Timer tm = (Timer)sender;
                                    tm.Dispose();
                                    pictureBox.Dispose();
                                    A.BackgroundImage =Properties.Resources.Base2;
                                    A.Tag = "GAMEOVER";
                                }
                            }
                        }

                    }
                }
            }
        }
    }
}
