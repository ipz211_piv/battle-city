﻿using System.Windows.Forms;
using System.Drawing;

namespace MainLibrary
{
    public class Tank
    {
        public PictureBox TankPictureBox;
        public bool TankMove = true;
        public bool TankUp = true;
        public bool TankDown = false;
        public bool TankRight = false;
        public bool TankLeft = false;
        public bool TankOnIce = false;
        int Speed = 5;
        public int Score = 0;
        public int HP = 4;

        public Tank() { 
        }
        public Tank(ref Panel gameSpace)
        {
            TankPictureBox = new PictureBox();
            TankPictureBox.Tag = "Tank";
            TankPictureBox.Size = new Size(50, 50);
            TankPictureBox.BackgroundImage = Properties.Resources.Tank_1_;
            TankPictureBox.Location = new Point(200, 600);
            
            gameSpace.Controls.Add(TankPictureBox);
            TankPictureBox.BackgroundImageLayout = ImageLayout.Stretch;
        }
        public Tank(Tank tank, ref Panel gameSpace)
        {
            this.HP = tank.HP;
            this.Score = tank.Score;
            TankPictureBox = new PictureBox();
            TankPictureBox.Tag = "Tank";
            TankPictureBox.Size = new Size(50, 50);
            TankPictureBox.BackgroundImage = Properties.Resources.Tank_1_;
            TankPictureBox.Location = new Point(200, 600);
            gameSpace.Controls.Add(TankPictureBox);
            TankPictureBox.BackgroundImageLayout = ImageLayout.Stretch;
        }
        public bool StopTankUp(Panel gameSpace, PictureBox Tank_pictureBox)
        {
            bool over = false;
            if (Tank_pictureBox.Top < 1)
                over = true;
            if (!over)
                foreach (Control OBJ in gameSpace.Controls)
                    if (OBJ.GetType() == typeof(PictureBox))
                    {
                        PictureBox obj = OBJ as PictureBox;
                        if (obj.Visible && (obj.Tag == "Brick" || obj.Tag == "Steel" ||
                            obj.Tag == "Water")
                            && Tank_pictureBox.Location.X < obj.Location.X + obj.Width
                            && Tank_pictureBox.Location.X > obj.Location.X - obj.Width * 2
                            && Tank_pictureBox.Location.Y == obj.Location.Y + obj.Height)
                            over = true;
                        if ((obj.Tag == "StandartEnemy" || obj.Tag == "FastEnemy" || obj.Tag == "ArmoredEnemy4" || obj.Tag == "ArmoredEnemy3" 
                            || obj.Tag == "ArmoredEnemy2" || obj.Tag == "ArmoredEnemy1" || obj.Tag == "PowerfullEnemy")
                           && Tank_pictureBox.Location.X < obj.Location.X + obj.Width
                            && Tank_pictureBox.Location.X > obj.Location.X - obj.Width
                            && Tank_pictureBox.Location.Y == obj.Location.Y + obj.Height)
                            over = true;
                    }

            return over;
        }
        public bool StopTankDown(Panel gameSpace, PictureBox Tank_pictureBox)
        {
            bool over = false;
            if (Tank_pictureBox.Top > gameSpace.Height - Tank_pictureBox.Height - 1)
                over = true;
            if (!over)
                foreach (Control OBJ in gameSpace.Controls)
                    if (OBJ.GetType() == typeof(PictureBox))
                    {
                        PictureBox obj = OBJ as PictureBox;
                        if (obj.Visible && (obj.Tag == "Brick" || obj.Tag == "Steel" ||
                            obj.Tag == "Water")
                            && Tank_pictureBox.Location.X < obj.Location.X + obj.Width
                            && Tank_pictureBox.Location.X > obj.Location.X - obj.Width * 2
                            && Tank_pictureBox.Location.Y == obj.Location.Y - obj.Height * 2)
                            over = true;
                        if ((obj.Tag == "StandartEnemy" || obj.Tag == "FastEnemy" || obj.Tag == "ArmoredEnemy4"|| obj.Tag == "ArmoredEnemy3" || obj.Tag == "ArmoredEnemy2" || obj.Tag == "ArmoredEnemy1" || obj.Tag == "PowerfullEnemy")
                            && Tank_pictureBox.Location.X < obj.Location.X + obj.Width
                            && Tank_pictureBox.Location.X > obj.Location.X - obj.Width 
                            && Tank_pictureBox.Location.Y == obj.Location.Y - obj.Height)
                            over = true;
                    }
            return over;
        }
        public bool StopTankRight(Panel gameSpace, PictureBox Tank_pictureBox)
        {
            bool over = false;
            if (Tank_pictureBox.Left > gameSpace.Width - Tank_pictureBox.Width-1)
                over = true;
            if (!over)
                foreach (Control OBJ in gameSpace.Controls)
                    if (OBJ.GetType() == typeof(PictureBox))
                    {
                        PictureBox obj = OBJ as PictureBox;
                        if (obj.Visible && (obj.Tag == "Brick" || obj.Tag == "Steel" ||
                            obj.Tag == "Water")
                            && Tank_pictureBox.Location.Y < obj.Location.Y + obj.Height
                            && Tank_pictureBox.Location.Y > obj.Location.Y - obj.Height * 2
                            && Tank_pictureBox.Location.X == obj.Location.X - obj.Height * 2)
                            over = true;
                        if ((obj.Tag == "StandartEnemy" || obj.Tag == "FastEnemy" || obj.Tag == "ArmoredEnemy4" || obj.Tag == "ArmoredEnemy3" || obj.Tag == "ArmoredEnemy2" || obj.Tag == "ArmoredEnemy1" || obj.Tag == "PowerfullEnemy") && Tank_pictureBox.Location.Y < obj.Location.Y + obj.Height
                            && Tank_pictureBox.Location.Y > obj.Location.Y - obj.Height
                            && Tank_pictureBox.Location.X == obj.Location.X - obj.Height)
                            over = true;
                    }
            return over;
        }
        public bool StopTankLeft(Panel gameSpace, PictureBox Tank_pictureBox)
        {
            bool over = false;
            if (Tank_pictureBox.Left < 1)
                over = true;
            if (!over)
                foreach (Control OBJ in gameSpace.Controls)
                    if (OBJ.GetType() == typeof(PictureBox))
                    {
                        PictureBox obj = OBJ as PictureBox;
                        if (obj.Visible && (obj.Tag == "Brick" || obj.Tag == "Steel" ||
                            obj.Tag == "Water")
                            && Tank_pictureBox.Location.Y < obj.Location.Y + obj.Height
                            && Tank_pictureBox.Location.Y > obj.Location.Y - obj.Height * 2
                            && Tank_pictureBox.Location.X == obj.Location.X + obj.Height)
                            over = true;
                        if ((obj.Tag == "StandartEnemy" || obj.Tag == "FastEnemy" || obj.Tag == "ArmoredEnemy4" || obj.Tag == "ArmoredEnemy3" || obj.Tag == "ArmoredEnemy2" || obj.Tag == "ArmoredEnemy1" || obj.Tag == "PowerfullEnemy")
                          && Tank_pictureBox.Location.Y < obj.Location.Y + obj.Height
                            && Tank_pictureBox.Location.Y > obj.Location.Y - obj.Height 
                            && Tank_pictureBox.Location.X == obj.Location.X + obj.Height)
                            over = true;
                    }
            return over;
        }
        public void Tank_KeyDown(KeyEventArgs e, Panel gameSpace, ref PictureBox Tank_pictureBox)
        {

            Image img = null;
            TankOnIce = false;

            foreach (Control Ice in gameSpace.Controls)
            {
                if (Ice.GetType() == typeof(PictureBox))
                {
                    PictureBox pictureBox = Ice as PictureBox;
                    if (pictureBox.Tag == "Ice"&& Tank_pictureBox.Bounds.IntersectsWith(pictureBox.Bounds))
                    {
                        TankOnIce = true;
                        break;
                    }
                }
            }
            if (TankMove)
            {
                img = Properties.Resources.Tank_1_;
                TankMove = false;
            }
            else
            {
                img = Properties.Resources.Tank_2_;
                TankMove = true;
            }

            if (e.KeyCode == Keys.Right)
            {
                TankUp = false;
                TankDown = false;
                TankRight = true;
                TankLeft = false;
                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                if (!StopTankRight(gameSpace, Tank_pictureBox))
                    Tank_pictureBox.Left = Tank_pictureBox.Left + Speed;
                if (TankOnIce&& !StopTankRight(gameSpace, Tank_pictureBox))
                    Tank_pictureBox.Left = Tank_pictureBox.Left + Speed;
            }
            if (e.KeyCode == Keys.Left)
            {
                TankUp = false;
                TankDown = false;
                TankRight = false;
                TankLeft = true;
                img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                if (!StopTankLeft(gameSpace, Tank_pictureBox))
                    Tank_pictureBox.Left = Tank_pictureBox.Left - Speed;
                if (TankOnIce&&!StopTankLeft(gameSpace, Tank_pictureBox))
                    Tank_pictureBox.Left = Tank_pictureBox.Left - Speed;
            }
            if (e.KeyCode == Keys.Up)
            {
                TankUp = true;
                TankDown = false;
                TankRight = false;
                TankLeft = false;
                if (!StopTankUp(gameSpace, Tank_pictureBox))
                    Tank_pictureBox.Top = Tank_pictureBox.Top - Speed;
                if (TankOnIce&& !StopTankUp(gameSpace, Tank_pictureBox))
                    Tank_pictureBox.Top = Tank_pictureBox.Top - Speed;
            }
            if (e.KeyCode == Keys.Down)
            {
                TankUp = false;
                TankDown = true;
                TankRight = false;
                TankLeft = false;
                img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                if (!StopTankDown(gameSpace, Tank_pictureBox))
                    Tank_pictureBox.Top = Tank_pictureBox.Top + Speed;
                if (TankOnIce&& !StopTankDown(gameSpace, Tank_pictureBox))
                    Tank_pictureBox.Top = Tank_pictureBox.Top + Speed;
            }
            Tank_pictureBox.BackgroundImage = img;

        }

    }
}
