﻿using System;

using System.Windows.Forms;
using System.Drawing;

namespace MainLibrary
{
    public class Enemy
    {
        Random random = new Random();
        public PictureBox EnemyPictureBox =new PictureBox();
       
        public bool EnemyMove = true;
        public bool EnemyUp = false;
        public bool EnemyDown = true;
        public bool EnemyRight = false;
        public bool EnemyLeft = false;
       
        public int TypeOfEnemy;
        public int EnemyPosition;
        public bool Alive = false;
        
        int direction=0;
        int directionTimer=5;

        bool EnemyBulletShooted = false;
        bool EnemyBulletUp = false;
        bool EnemyBulletDown = false;
        bool EnemyBulletLeft = false;
        bool EnemyBulletRight = false;
        Panel GameSpace;
        PictureBox BulletpictureBox;
        Timer EnemyBulletTimer;
        Timer EnemyTimer;
        Timer EnemyTimerBang;
        GameSound sound = new GameSound();
        Explosion explosion;
        
        public Enemy()
        {
        }
        public Enemy(ref Panel gameSpace)
        {

            EnemyPictureBox.Size = new Size(50, 50);
            TypeOfEnemy = random.Next(1, 11);
            if (TypeOfEnemy>= 1&& TypeOfEnemy<=5)
            {
                EnemyPictureBox.BackgroundImage = Properties.Resources.StandartEnemy_1_;
                EnemyPictureBox.Tag = "StandartEnemy";
            }
            if (TypeOfEnemy >= 6 && TypeOfEnemy <= 7)
            {
                EnemyPictureBox.BackgroundImage = Properties.Resources.FastEnemy_1_;
                EnemyPictureBox.Tag = "FastEnemy";
            }
            if (TypeOfEnemy >= 8 && TypeOfEnemy <= 9)
            {
                EnemyPictureBox.BackgroundImage = Properties.Resources.PowerfullEnemy_1_;
                EnemyPictureBox.Tag = "PowerfullEnemy";
            }
            if (TypeOfEnemy == 10) {
                EnemyPictureBox.BackgroundImage = Properties.Resources.ArmoredEnemy_1_3;
                EnemyPictureBox.Tag = "ArmoredEnemy4";
            }
            
            //red-1enemy
            //green-2enemy
            //blue-3enemy
            //aqua-4enemy
            bool red =true, green = true, blue = true, aqua = true;
            foreach (Control OBJ in gameSpace.Controls)
                if (OBJ.GetType() == typeof(PictureBox))
                {
                    PictureBox pb = OBJ as PictureBox;
                    if ((pb.Tag == "StandartEnemy" || pb.Tag == "FastEnemy" || pb.Tag == "ArmoredEnemy4"
                        || pb.Tag == "ArmoredEnemy3" || pb.Tag == "ArmoredEnemy2" || pb.Tag == "ArmoredEnemy1" || pb.Tag == "PowerfullEnemy")
                        && !pb.BackColor.IsEmpty)
                    {
                        if (pb.BackColor == Color.Red)
                            red = false;
                        
                        if (pb.BackColor == Color.Green)
                            green = false;
                        
                        if (pb.BackColor == Color.Blue)
                            blue = false;
                        
                        if (pb.BackColor == Color.Aqua)
                            aqua = false;
                    }

                } 
            if(red) EnemyPictureBox.BackColor = Color.Red;
            else if (green) EnemyPictureBox.BackColor = Color.Green;
            else if (blue) EnemyPictureBox.BackColor = Color.Blue;
            else if (aqua) EnemyPictureBox.BackColor = Color.Aqua;

            EnemyPosition = random.Next(0, 3);
            if (EnemyPosition == 0)
                EnemyPictureBox.Location = new Point(0, 0);
            else if (EnemyPosition == 1)
                EnemyPictureBox.Location = new Point(300, 0);
            else if (EnemyPosition == 2)
                EnemyPictureBox.Location = new Point(600, 0);

            gameSpace.Controls.Add(EnemyPictureBox);
            EnemyPictureBox.BackgroundImageLayout = ImageLayout.Stretch;

            Alive= true;
            EnemyTimer = new Timer();
            EnemyTimer.Interval = 45;
            GameSpace = gameSpace;
            EnemyTimer.Tick += new EventHandler(EnemyTimerTick);
            EnemyTimer.Start();

            EnemyBulletGo();
            EnemyTimerBang = new Timer();
            EnemyTimerBang.Interval = random.Next(10, 30) * 100;
            EnemyTimerBang.Tick += new EventHandler(EnemyTimerBangTick);
            EnemyTimerBang.Start();

        }
       
        public void EnemyTimerTick(object sender, EventArgs e)
        {
            if (EnemyPictureBox.Tag == "Dead")
            {
                EnemyTimer.Stop();
                EnemyPictureBox.BackColor = Color.White;
                BulletpictureBox.Dispose();
            }
            else
            {
                Image img = null;
                if (EnemyMove)
                {
                    if (TypeOfEnemy >= 1 && TypeOfEnemy <= 5)
                        img = Properties.Resources.StandartEnemy_1_;
                    if (TypeOfEnemy >= 6 && TypeOfEnemy <= 7)
                        img = Properties.Resources.FastEnemy_1_;
                    if (TypeOfEnemy == 10)
                        if (EnemyPictureBox.Tag == "ArmoredEnemy4")
                        {
                            img = Properties.Resources.ArmoredEnemy_1_3;
                        }
                        else if (EnemyPictureBox.Tag == "ArmoredEnemy3")
                            img = Properties.Resources.ArmoredEnemy_1_2;
                        else if (EnemyPictureBox.Tag == "ArmoredEnemy2")
                            img = Properties.Resources.ArmoredEnemy_1_1;
                        else if (EnemyPictureBox.Tag == "ArmoredEnemy1")
                            img = Properties.Resources.ArmoredEnemy_1_;
                    if (TypeOfEnemy >= 8 && TypeOfEnemy <= 9)
                        img = Properties.Resources.PowerfullEnemy_1_;

                    EnemyMove = false;
                }
                else
                {
                    if (TypeOfEnemy >= 1 && TypeOfEnemy <= 5)
                        img = Properties.Resources.StandartEnemy_2_;
                    if (TypeOfEnemy >= 6 && TypeOfEnemy <= 7)
                        img = Properties.Resources.FastEnemy_2_;
                    if (TypeOfEnemy == 10)
                        if (EnemyPictureBox.Tag == "ArmoredEnemy4")
                            img = Properties.Resources.ArmoredEnemy_2_3;
                        else if (EnemyPictureBox.Tag == "ArmoredEnemy3")
                            img = Properties.Resources.ArmoredEnemy_2_2;
                        else if (EnemyPictureBox.Tag == "ArmoredEnemy2")
                            img = Properties.Resources.ArmoredEnemy_2_1;
                        else if (EnemyPictureBox.Tag == "ArmoredEnemy1")
                            img = Properties.Resources.ArmoredEnemy_2_;
                    if (TypeOfEnemy >= 8 && TypeOfEnemy <= 9)
                        img = Properties.Resources.PowerfullEnemy_2_;
                    EnemyMove = true;
                }

                if (directionTimer <= 0)
                {
                    direction = random.Next(0, 4);
                    directionTimer = random.Next(25, 35);
                }
                if (direction == 0)
                {
                    EnemyMovingUp(ref img);
                    if (StopEnemyUp(GameSpace, EnemyPictureBox))
                        directionTimer = 0;
                    directionTimer--;
                }
                if (direction == 1)
                {
                    EnemyMovingRight(ref img);
                    if (StopEnemyRight(GameSpace, EnemyPictureBox))
                        directionTimer = 0;
                    directionTimer--;
                }
                if (direction == 2)
                {
                    EnemyMovingLeft(ref img);
                    if (StopEnemyLeft(GameSpace, EnemyPictureBox))
                        directionTimer = 0;
                    directionTimer--;
                }
                if (direction == 3)
                {
                    EnemyMovingDown(ref img);
                    if (StopEnemyDown(GameSpace, EnemyPictureBox))
                        directionTimer = 0;
                    directionTimer--;
                }
                EnemyPictureBox.BackgroundImage = img;
            }
            
        }
       

        public void EnemyMovingUp(ref Image img)
        {
            if(!StopEnemyUp( GameSpace, EnemyPictureBox)) {
                EnemyPictureBox.Top -= 5;
                EnemyDown = false;
                EnemyLeft = false;
                EnemyRight = false;
                EnemyUp = true;
            }
            if ((TypeOfEnemy >= 6 && TypeOfEnemy <= 7) && !StopEnemyUp(GameSpace, EnemyPictureBox))
                EnemyPictureBox.Top -= 5;
        }
        public void EnemyMovingDown(ref Image img)
        {
            if (!StopEnemyDown(GameSpace, EnemyPictureBox)) {
                EnemyPictureBox.Top += 5;
                EnemyDown = true;
                EnemyLeft = false;
                EnemyRight = false;
                EnemyUp = false;
                img.RotateFlip(RotateFlipType.Rotate180FlipNone);
            }
            if ((TypeOfEnemy >= 6 && TypeOfEnemy <= 7) && !StopEnemyDown(GameSpace, EnemyPictureBox))
                EnemyPictureBox.Top += 5;

        }
        public void EnemyMovingRight(ref Image img)
        {
            if (!StopEnemyRight(GameSpace, EnemyPictureBox))
            {
                EnemyPictureBox.Left += 5;
                EnemyDown = false;
                EnemyLeft = false;
                EnemyRight = true;
                EnemyUp = false;
                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            if ((TypeOfEnemy >= 6 && TypeOfEnemy <= 7) && !StopEnemyRight(GameSpace, EnemyPictureBox))
                EnemyPictureBox.Left += 5;
        }
        public void EnemyMovingLeft(ref Image img)
        {
            if (!StopEnemyLeft(GameSpace, EnemyPictureBox))
            {
                EnemyPictureBox.Left -= 5;
                EnemyDown = false;
                EnemyLeft = true;
                EnemyRight = false;
                EnemyUp = false;
                img.RotateFlip(RotateFlipType.Rotate270FlipNone);
            }
            if ((TypeOfEnemy >= 6 && TypeOfEnemy <= 7) && !StopEnemyLeft(GameSpace, EnemyPictureBox))
                EnemyPictureBox.Left -= 5;

        }

        public void EnemyTimerBangTick(object sender, EventArgs e)
        {
            if (EnemyPictureBox.Tag == "Dead")
                EnemyTimerBang.Stop();
            else
                EnemyBulletGo();
        }
        public void EnemyBulletGo()
        {
            if (!EnemyBulletShooted)
            {
                sound.FirePlay();
                EnemyBulletShooted = true;
                EnemyShot();
            }
        }
        public void EnemyShot()
        {

            Image img = Properties.Resources.Bullet;
            BulletpictureBox = new PictureBox();
            EnemyBulletDown = false;
            EnemyBulletUp = false;
            EnemyBulletLeft = false;
            EnemyBulletRight = false;
            if (EnemyUp)
            {
                BulletpictureBox.Size = new Size(6, 10);
                BulletpictureBox.Location = new Point(EnemyPictureBox.Left + EnemyPictureBox.Width / 2 - 3, EnemyPictureBox.Top + EnemyPictureBox.Height / 2);
                EnemyBulletUp = true;
            }
            if (EnemyDown)
            {
                BulletpictureBox.Size = new Size(6, 10);
                BulletpictureBox.Location = new Point(EnemyPictureBox.Left + EnemyPictureBox.Width / 2 - 3, EnemyPictureBox.Top + EnemyPictureBox.Height / 2);
                img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                EnemyBulletDown = true;
            }
            if (EnemyLeft)
            {
                BulletpictureBox.Size = new Size(10, 6);
                BulletpictureBox.Location = new Point(EnemyPictureBox.Left, EnemyPictureBox.Top + EnemyPictureBox.Height / 2 - 3);
                img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                EnemyBulletLeft = true;
            }
            if (EnemyRight)
            {
                BulletpictureBox.Size = new Size(10, 6);
                BulletpictureBox.Location = new Point(EnemyPictureBox.Left, EnemyPictureBox.Top + EnemyPictureBox.Height / 2 - 3);
                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                EnemyBulletRight = true;
            }

            BulletpictureBox.BackgroundImage = img;
            BulletpictureBox.BackgroundImageLayout = ImageLayout.Stretch;
            BulletpictureBox.Tag = "EnemyBullet";
            GameSpace.Controls.Add(BulletpictureBox);
            BulletpictureBox.BringToFront();
            EnemyPictureBox.BringToFront();
            foreach (Control time in GameSpace.Controls)
            {
                if (time.GetType() == typeof(PictureBox))
                {
                    PictureBox pictureBox = time as PictureBox;
                    if (pictureBox.Tag == "Tree")
                    {
                        pictureBox.BringToFront();
                    }
                }
            }

            if (EnemyPictureBox.BackColor == Color.Red) BulletpictureBox.BackColor = Color.Red;
            else if (EnemyPictureBox.BackColor == Color.Green) BulletpictureBox.BackColor = Color.Green;
            else if (EnemyPictureBox.BackColor == Color.Blue) BulletpictureBox.BackColor = Color.Blue;
            else if (EnemyPictureBox.BackColor == Color.Aqua) BulletpictureBox.BackColor = Color.Aqua;
            //gamespace = gameSpace;
            //enemY = enemy;

            EnemyBulletTimer = new Timer();
            EnemyBulletTimer.Interval = 20;
            EnemyBulletTimer.Tick += new EventHandler(EnemyBulletTimerTick);
            EnemyBulletTimer.Start();
        }
        public void EnemyBulletTimerTick(object sender, EventArgs e)
        {
            if (EnemyPictureBox.Tag == "Dead")
            {
                EnemyBulletTimer.Stop();
                BulletpictureBox.BackColor = Color.Transparent;
            }

            Timer tm = (Timer)sender;

            foreach (Control bullet in GameSpace.Controls)
            {

                if (bullet.GetType() == typeof(PictureBox))
                {
                    PictureBox pictureBox = bullet as PictureBox;
                    if (pictureBox.Tag == "EnemyBullet" && EnemyPictureBox.BackColor == pictureBox.BackColor)
                    {

                        if (EnemyBulletUp)
                        {
                            pictureBox.Top = pictureBox.Top - 10;
                            if (EnemyPictureBox.Tag == "PowerfullEnemy")
                                pictureBox.Top = pictureBox.Top - 10;
                        }
                        if (EnemyBulletDown)
                        {
                            pictureBox.Top = pictureBox.Top + 10;
                            if (EnemyPictureBox.Tag == "PowerfullEnemy")
                                pictureBox.Top = pictureBox.Top + 10;
                        }
                        if (EnemyBulletRight)
                        {
                            pictureBox.Left = pictureBox.Left + 10;
                            if (EnemyPictureBox.Tag == "PowerfullEnemy")
                                pictureBox.Left = pictureBox.Left + 10;
                        }
                        if (EnemyBulletLeft)
                        {
                            pictureBox.Left = pictureBox.Left - 10;
                            if (EnemyPictureBox.Tag == "PowerfullEnemy")
                                pictureBox.Left = pictureBox.Left - 10;
                        }
                        if (pictureBox.Top < 0 || pictureBox.Top > GameSpace.Height - EnemyPictureBox.Height / 2 || pictureBox.Left < 0 || pictureBox.Left > GameSpace.Width - EnemyPictureBox.Width / 2)
                        {
                            EnemyBulletDown = false;
                            EnemyBulletUp = false;
                            EnemyBulletLeft = false;
                            EnemyBulletRight = false;
                            EnemyBulletShooted = false;
                            explosion = new Explosion(ref GameSpace, pictureBox.Location);
                            tm.Dispose();
                            pictureBox.Dispose();
                        }
                        foreach (Control A in GameSpace.Controls)
                        {

                            if (A.GetType() == typeof(PictureBox))
                            {
                                PictureBox NewpictureBox = A as PictureBox;
                                if (NewpictureBox.Visible && (NewpictureBox.Tag == "Brick")
                                    && pictureBox.Bounds.IntersectsWith(NewpictureBox.Bounds))
                                {

                                    EnemyBulletShooted = false;

                                    explosion = new Explosion(ref GameSpace, pictureBox.Location);
                                    BulletpictureBox.Tag = "";
                                    tm.Dispose();
                                    pictureBox.Dispose();
                                    NewpictureBox.Dispose();
                                }
                                if (NewpictureBox.Visible && (NewpictureBox.Tag == "Steel")
                                    && pictureBox.Bounds.IntersectsWith(NewpictureBox.Bounds))
                                {

                                    EnemyBulletShooted = false;
                                    BulletpictureBox.Tag = "";
                                    explosion = new Explosion(ref GameSpace, pictureBox.Location);
                                    tm.Dispose();
                                    pictureBox.Dispose();
                                }
                                if (NewpictureBox.Visible && NewpictureBox.Tag == "Tank"
                                    && pictureBox.Bounds.IntersectsWith(NewpictureBox.Bounds))
                                {

                                    EnemyBulletShooted = false;
                                    BulletpictureBox.Tag = "";
                                    sound.DestroyedPlay();
                                    tm.Dispose();
                                    sound.DestroyedPlay();
                                    explosion = new Explosion(ref GameSpace, NewpictureBox.Location, true);
                                    pictureBox.Dispose();
                                    NewpictureBox.Dispose();
                                }

                            }
                            if (A.GetType() == typeof(WatermelonBase))
                            {
                                if (A.Visible && pictureBox.Bounds.IntersectsWith(A.Bounds))
                                {
                                    EnemyBulletShooted = false;
                                    BulletpictureBox.Tag = "";
                                    tm.Dispose();
                                    pictureBox.Dispose();
                                    A.BackgroundImage = Properties.Resources.Base2;
                                    A.Tag = "GAMEOVER";
                                }
                            }
                        }
                    }
                }
            }
        }

        public bool StopEnemyUp(Panel GameSpace, PictureBox EnemypictureBox)
        {
            bool over = false;
            if (EnemypictureBox.Top < 1)
                over = true;
            if (!over)
                foreach (Control OBJ in GameSpace.Controls)
                    if (OBJ.GetType() == typeof(PictureBox))
                    {
                        PictureBox obj = OBJ as PictureBox;
                        if (obj.Visible && (obj.Tag == "Brick" || obj.Tag == "Steel" ||
                            obj.Tag == "Water")
                            && EnemypictureBox.Location.X < obj.Location.X + obj.Width
                            && EnemypictureBox.Location.X > obj.Location.X - obj.Width * 2
                            && EnemypictureBox.Location.Y == obj.Location.Y + obj.Height)
                            over = true;
                        if ((obj.Tag == "Tank" || obj.Tag == "StandartEnemy" || obj.Tag == "FastEnemy" || obj.Tag == "ArmoredEnemy1"
                            || obj.Tag == "ArmoredEnemy2" || obj.Tag == "ArmoredEnemy3" || obj.Tag == "ArmoredEnemy4"|| obj.Tag == "PowerfullEnemy")
                            && EnemypictureBox.Location.X < obj.Location.X + obj.Width
                            && EnemypictureBox.Location.X > obj.Location.X - obj.Width
                            && EnemypictureBox.Location.Y == obj.Location.Y + obj.Height)
                            over = true;
                    }

            return over;
        }
        public bool StopEnemyDown(Panel GameSpace, PictureBox EnemypictureBox)
        {
            bool over = false;
            if (EnemypictureBox.Top > GameSpace.Height - EnemypictureBox.Height - 1)
                over = true;
            if (!over)
                foreach (Control OBJ in GameSpace.Controls)
                    if (OBJ.GetType() == typeof(PictureBox))
                    {
                        PictureBox obj = OBJ as PictureBox;
                        if (obj.Visible && (obj.Tag == "Brick" || obj.Tag == "Steel" ||
                            obj.Tag == "Water")
                            && EnemypictureBox.Location.X < obj.Location.X + obj.Width
                            && EnemypictureBox.Location.X > obj.Location.X - obj.Width * 2
                            && EnemypictureBox.Location.Y == obj.Location.Y - obj.Height * 2)
                            over = true;
                        if ((obj.Tag == "Tank" || obj.Tag == "StandartEnemy" || obj.Tag == "FastEnemy" || obj.Tag == "ArmoredEnemy1"
                            || obj.Tag == "ArmoredEnemy2" || obj.Tag == "ArmoredEnemy3" || obj.Tag == "ArmoredEnemy4" || obj.Tag == "PowerfullEnemy")
                            && EnemypictureBox.Location.X < obj.Location.X + obj.Width
                            && EnemypictureBox.Location.X > obj.Location.X - obj.Width 
                            && EnemypictureBox.Location.Y == obj.Location.Y - obj.Height )
                            over = true;
                    }
            return over;
        }
        public bool StopEnemyRight(Panel GameSpace, PictureBox EnemypictureBox)
        {
            bool over = false;
            if (EnemypictureBox.Left > GameSpace.Width - EnemypictureBox.Width-1)
                over = true;
            if (!over)
                foreach (Control OBJ in GameSpace.Controls)
                    if (OBJ.GetType() == typeof(PictureBox))
                    {
                        PictureBox obj = OBJ as PictureBox;
                        if (obj.Visible && (obj.Tag == "Brick" || obj.Tag == "Steel" ||
                            obj.Tag == "Water" || obj.Tag == "Tank")
                            && EnemypictureBox.Location.Y < obj.Location.Y + obj.Height
                            && EnemypictureBox.Location.Y > obj.Location.Y - obj.Height * 2
                            && EnemypictureBox.Location.X == obj.Location.X - obj.Height * 2)
                            over = true;
                        if((obj.Tag == "Tank" || obj.Tag == "StandartEnemy" || obj.Tag == "FastEnemy" || obj.Tag == "ArmoredEnemy1"
                            || obj.Tag == "ArmoredEnemy2" || obj.Tag == "ArmoredEnemy3" || obj.Tag == "ArmoredEnemy4" || obj.Tag == "PowerfullEnemy")
                            && EnemypictureBox.Location.Y < obj.Location.Y + obj.Height
                            && EnemypictureBox.Location.Y > obj.Location.Y - obj.Height 
                            && EnemypictureBox.Location.X == obj.Location.X - obj.Height)
                            over = true;
                    }
            return over;
        }
        public bool StopEnemyLeft(Panel GameSpace, PictureBox EnemypictureBox)
        {
            bool over = false;
            if (EnemypictureBox.Left < 1)
                over = true;
            if (!over)
                foreach (Control OBJ in GameSpace.Controls)
                    if (OBJ.GetType() == typeof(PictureBox))
                    {
                        PictureBox obj = OBJ as PictureBox;
                        if (obj.Visible && (obj.Tag == "Brick" || obj.Tag == "Steel" ||
                            obj.Tag == "Water" || obj.Tag == "Tank")
                            && EnemypictureBox.Location.Y < obj.Location.Y + obj.Height
                            && EnemypictureBox.Location.Y > obj.Location.Y - obj.Height * 2
                            && EnemypictureBox.Location.X == obj.Location.X + obj.Height)
                            over = true;
                        if((obj.Tag == "Tank" || obj.Tag == "StandartEnemy" || obj.Tag == "FastEnemy"|| obj.Tag == "ArmoredEnemy1"
                            ||obj.Tag == "ArmoredEnemy2"|| obj.Tag == "ArmoredEnemy3"|| obj.Tag == "ArmoredEnemy4" || obj.Tag == "PowerfullEnemy") 
                            && EnemypictureBox.Location.Y < obj.Location.Y + obj.Height
                            && EnemypictureBox.Location.Y > obj.Location.Y - obj.Height 
                            && EnemypictureBox.Location.X == obj.Location.X + obj.Height)
                            over = true;
                    }
            return over;
        }
    }
}
