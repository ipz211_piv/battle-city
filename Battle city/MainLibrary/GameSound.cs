﻿using System.Media;

namespace MainLibrary
{
    public class GameSound
    {
        SoundPlayer player;
        public void FirePlay()
        {
            player = new SoundPlayer(Properties.Resources.Fire);
            player.Play();
        }
        public void BlockColissionPlay()
        {
            player = new SoundPlayer(Properties.Resources.BlockColission);
            player.Play();
        }
        public void ArmoredPenetrationPlay()
        {
            player = new SoundPlayer(Properties.Resources.ArmoredPenetration);
            player.Play();
        }
        public void DestroyedPlay()
        {
            player = new SoundPlayer(Properties.Resources.Destroyed);
            player.Play();
        }
    }
}
