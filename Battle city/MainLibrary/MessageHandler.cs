﻿using System.Windows.Forms;
using System.Drawing;

namespace MainLibrary
{
    public class MessageHandler:PictureBox
    {
        public void GAMEOVER(ref Panel gameSpace)
        {
            this.Size = new Size(258, 142);
            this.Location = new Point(0,0);
            
            this.Image = Properties.Resources.GameOver;
            this.Tag = "GIF";
            gameSpace.Controls.Add(this);
            this.BringToFront();
        }
        public void WIN(ref Panel gameSpace)
        {
            this.Size = new Size(258, 144);
            this.Location = new Point(0, 0);
            this.Tag = "GIF";
            this.Image = Properties.Resources.Win;

            gameSpace.Controls.Add(this);
            this.BringToFront();

        }
        public void Clear(ref Panel gameSpace)
        {
            this.Size = new Size(0,0);
            

        }
    }
}
